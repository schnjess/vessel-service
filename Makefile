build:
	protoc --plugin=protoc-gen-go=${GOPATH}/bin/protoc-gen-go \
	--plugin=protoc-gen-micro=${GOPATH}/bin/protoc-gen-micro \
	--proto_path=${GOPATH}/src:. --micro_out=. \
	--go_out=. proto/vessel/vessel.proto
	docker build -t vessel-service .

run:
	docker run --net="host" \
	-p 50053 \
	-e MICRO_SERVER_ADDRESS=:50053 \
	-e MICRO_REGISTRY=mdns \
	vessel-service
