# vessel-service/Dockerfile
FROM golang:1.11.0 as builder

WORKDIR /go/src/gitlab.com/schnjess/micros/vessel-service

COPY . .

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .


FROM debian:latest

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/schnjess/micros/vessel-service/vessel-service .

CMD ["./vessel-service"]
